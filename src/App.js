import React, { useState } from "react";
import "./App.css";
import AddPatient from "./components/AddPatient";
import PatientList from "./components/PatientList";

// function AddPatientForm({ addPatient }) {
//   const [value, setValue] = useState("");

//   const handleSubmit = e => {
//     e.preventDefault();
//     if (!value) {
//       return;
//     }
//     addPatient(value);
//     setValue("");
//   };

//   return (
//     <Form onSubmit={handleSubmit}>
//       <FormGroup>
//         <FormControl
//           placeholder="First name..."
//           value={value}
//           onChange={e => setValue(e.target.value)}
//         />
//       </FormGroup>
//     </Form>
//   );
// }

function App() {
  //patientArr = state
  //setTodos = method used to update the state
  const [patientArr, setPatient] = useState([
    {
      first_name: "pierre-emerick",
      last_name: "aubameyang",
      date_of_birth: "01/01/91",
      gender: "male",
      isDeactivated: false
    },
    {
      first_name: "alexander",
      last_name: "lacazette",
      date_of_birth: "02/02/92",
      gender: "male",
      isDeactivated: false
    },
    {
      first_name: "nicolas",
      last_name: "pepe",
      date_of_birth: "03/03/93",
      gender: "male",
      isDeactivated: false
    }
  ]);

  const addPatient = first_name => {
    //...todos is using a spread operator to copy whats in the array and add in 'text'
    const newPatientArr = [...patientArr, { first_name }];
    setPatient(newPatientArr);
  };

  const deactivatePatient = index => {
    const newPatientArr = [...patientArr];
    newPatientArr[index].isDeactivated = true;
    setPatient(newPatientArr);
  };

  const activatePatient = index => {
    const newPatientArr = [...patientArr];
    newPatientArr[index].isDeactivated = false;
    setPatient(newPatientArr);
  };

  const removePatient = index => {
    const newPatientArr = [...patientArr];
    newPatientArr.splice(index, 1);
    setPatient(newPatientArr);
  };

  return (
    <div id="speech-pathologist-companion" className="app">
      <h1>Speech Pathologist Companion</h1>
      <div className="patient-list">
        {patientArr.map((patient, index) => (
          <PatientList
            key={index}
            index={index}
            patient={patient}
            deactivatePatient={deactivatePatient}
            activatePatient={activatePatient}
            removePatient={removePatient}
          />
        ))}
        <AddPatient addPatient={addPatient} />
      </div>
    </div>
  );
}

export default App;
