import React, { useState } from "react";
import Form from "react-bootstrap/Form";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";

function AddPatient({ addPatient }) {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [dateOfBirth, setDateOfBirth] = useState("");

  const handleSubmit = e => {
    console.log("submitting");
    e.preventDefault();
    if (!firstName || !lastName || !dateOfBirth) {
      console.log("something is null");
      return;
    }
    addPatient(firstName);
    setFirstName("");
    setLastName("");
    setDateOfBirth("");
  };

  return (
    <Form>
      <Form.Row>
        <Form.Group as={Col} controlId="formGridFirstName">
          <Form.Control
            placeholder="First name.."
            value={firstName}
            onChange={e => setFirstName(e.target.value)}
          />
        </Form.Group>

        <Form.Group as={Col} controlId="formGridLastName">
          <Form.Control
            placeholder="Last name.."
            value={lastName}
            onChange={e => setLastName(e.target.value)}
          />
        </Form.Group>
      </Form.Row>

      <Form.Row>
        <Form.Group as={Col} controlId="formGridDay">
          <Form.Control as="select">
            <option>...</option>
          </Form.Control>
        </Form.Group>
        <Form.Group as={Col} controlId="formGridMonth">
          <Form.Control as="select">
            <option>...</option>
          </Form.Control>
        </Form.Group>
        <Form.Group as={Col} controlId="formGridYear">
          <Form.Control as="select">
            <option>...</option>
          </Form.Control>
        </Form.Group>
      </Form.Row>
      <Button variant="primary" type="submit" onSubmit={handleSubmit}>
        Save Patient
      </Button>
    </Form>

    // <Form onSubmit={handleSubmit}>
    //   <FormGroup>
    //     <FormControl
    //       placeholder="First name..."
    //       value={firstName}
    //       onChange={e => setFirstName(e.target.value)}
    //     />
    //     <FormControl
    //       placeholder="Last name..."
    //       value={lastName}
    //       onChange={e => setLastName(e.target.value)}
    //     />
    //     <FormControl
    //       placeholder="Date of birth..."
    //       value={dateOfBirth}
    //       onChange={e => setDateOfBirth(e.target.value)}
    //     />
    //   </FormGroup>
    // </Form>
  );
}

export default AddPatient;
