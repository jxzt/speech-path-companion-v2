import React from "react";
import Button from "react-bootstrap/Button";

function PatientList({
  patient,
  index,
  deactivatePatient,
  activatePatient,
  removePatient
}) {
  let activeClasses = "patient-card";
  let activateButtonDisabled = true;
  let deactivateButtonDisabled = false;
  if (patient.isDeactivated) {
    activeClasses += " disabled-patient";
    console.log(activeClasses);
    activateButtonDisabled = false;
    deactivateButtonDisabled = true;
  } else {
    activateButtonDisabled = true;
    deactivateButtonDisabled = false;
  }

  return (
    <div
      // style={{ textDecoration: todo.isDeactivated ? "line-through" : "" }}
      className={activeClasses}
    >
      <div className="patient-info-group">
        {patient.first_name} {patient.last_name}
        <br />
        {patient.date_of_birth}
      </div>
      <div className="button-group">
        <Button
          id="activate-patient-button"
          variant="success"
          onClick={() => activatePatient(index)}
          disabled={activateButtonDisabled}
        >
          Activate
        </Button>
        <Button
          id="deactivate-patient-button"
          variant="warning"
          onClick={() => deactivatePatient(index)}
          disabled={deactivateButtonDisabled}
        >
          Deactivate
        </Button>
        <Button
          id="remove-patient-button"
          variant="danger"
          onClick={() => removePatient(index)}
        >
          X
        </Button>
      </div>
    </div>
  );
}

export default PatientList;
